# Random scripts

## Weechat

- [urltitel](weechat/urltitel.py): a shitty script to autosend web page title of
  URLs.

## Shell

- [nvim-alt](shell/nvim-alt.bash): run nvim with an alternative configuration.
