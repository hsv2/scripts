#!/bin/bash
# vi: et sw=4 ts=4:

if [[ -z "$(LC_ALL=C type -t realpath)" ]]
then
    realpath() (
    OURPWD=$PWD
    cd "$(dirname "$1")"
    LINK=$(readlink "$(basename "$1")")
    while [ "$LINK" ]; do
        cd "$(dirname "$LINK")"
        LINK=$(readlink "$(basename "$1")")
    done
    REALPATH="$PWD/$(basename "$1")"
    cd "$OURPWD"
    echo "$REALPATH"
)
fi

if [[ $TERMUX_VERSION ]];
then
    TMPDIR="$TMUX_TMPDIR"
else
    TMPDIR="${TMPDIR:-/tmp}"
fi

if [[ ! -f "$(realpath $1)" ]]
then
    echo "Invalid argument"
    echo "syntax: $0 VIMRC [DIR] [NVIM ARGS]"
    echo "VIMRC is the configuration file to use"
    echo "DIR is where runtime and configuration will be stored."
    echo "if not specified a temporary one will be used."
fi

if [[ -z "$2" ]]
then
    NVIM_DIR="$(mktemp --directory hsv2.nvim-test.XXXXXXXX)"
else
    if [[ ! -d "$(realpath $2)" ]]
    then
        mkdir "$2"
    fi
    NVIM_DIR="$2"
fi

export XDG_DATA_HOME="$NVIM_DIR/data"
export XDG_CONFIG_HOME="$NVIM_DIR/conf"
export MYVIMRC="$1"

echo data stored in $NVIM_DIR
nvim -u "$MYVIMRC" ${@:3}
